/*******************************************************************
 * Zakladne definicie ostrova a funkcie na pracu s nim
 *
 *******************************************************************/

#ifndef __ISLE_H
#define __ISLE_H

#include <stdio.h>

#define ERR_OK        0
#define ERR_NO_POP    -1
#define ERR_NO_FOOD   -2
#define ERR_NO_WOOD   -3
#define ERR_NO_ROOM   -4
#define ERR_NO_FOREST -5

#define MIN(X, Y) ((X)<(Y) ? (X) : (Y))
#define MAX(X, Y) ((X)<(Y) ? (Y) : (X))

#define MAX_ISLE_NAME  30

typedef struct _settings_t {
   int FoodPerFieldMin, FoodPerFieldMax;
   int BadYearOffset, BadYearChance;
   int WoodLossPercent, FoodLossPercent;
   int PopPerFarm, WoodPerFarm;
   int PopPerVillage, MaxPopPerVillage, WoodPerVillage;
   int MoaiMultiplier;
   
   int BuildAccidentChance;        //Village build: per half village population, 
                                   //Moai build: per population
   int ChopForestAccidentChance;   //per village population * chopped forests
   int FishingAccidentChance;      //per total population
  
   int FoodGatherChance, WoodGatherChance, FishingChance;  
   int WoodPerBoat, PopPerBoat;
   
   int GrowthRate, EmigrationRate;
   int StarvationChance;
} Settings;

extern Settings GlobalSettings;

//Global and local settings macro
// GS macro can be used anywhere, accesses GlobalSettings structure
// LS macro can only be used in function that has pointer to isle called pIsle
// IS macro can only be used with any pointer to isle
#define GS(X) (GlobalSettings.X)
#define LS(X) (pIsle->pSettings->X)
#define IS(PTR, X) ((PTR)->pSettings->X)

typedef struct _isle_t {
    char name[MAX_ISLE_NAME+1];

    //max 100 total
    int forests;        
    int villages;       
    int farms;          

    int pop;
    int rats;
    
    int food;
    int wood;

    int moai;
    
    Settings *pSettings;
} Isle;

int debug_print_isle(FILE* f, const Isle *pIsle);
//TODO: pretty_print_isle
//TODO: serialize
//TODO: deserialize

//vrati pocet volnych miest na ostrove
int get_empty(const Isle *pIsle);

//actions
//new type: pointer to action, to simplify notation in testing
typedef int (*VillageAction)(Isle*);

int va_build_farm(Isle* pIsle); 
int va_build_village(Isle* pIsle);
int va_chop_forest(Isle* pIsle);
int va_gather_wood(Isle* pIsle);
int va_gather_food(Isle* pIsle);
int va_go_fishing(Isle* pIsle);
int va_build_moai(Isle* pIsle);
int va_do_nothing(Isle* pIsle);

extern VillageAction VillageActions[];
extern const char* VillageActionNames[];
#define NUM_ACTIONS 8

//yearly update, return number of emigrants
int yearly_update(Isle* pIsle);


#endif //__ISLE_H
