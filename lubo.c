/**************************************************************************
 * Zadanie z AaP: Simulacia ostrovnej ekonomiky
 * Kostra zadania: (C) Pavol Zajac, 2014
 *
 * Riesenie vypracoval: 
 *    [PRIEZVISKO MENO, ID, KRUZOK]
 *
 * Ulohy (napiste [X] pred komplet splnenu ulohu, [-] pred ciastocne riesenie):
 *  [ ] 1. Inicializacia pola n ostrovov, n voli pouzivatel (dynamicke pole)
 *  [ ] 2. Serializacia stavu (nacitanie/zapis do suboru)
 *  [ ] 3. Implementacia migracie a/alebo skusenosti
 *  [ ] 4. User interface
 *  [ ] 5. Umela inteligencia
 *
 * Extra body:
 *    [NAPISTE, CO EXTRA STE IMPLEMENTOVALI]
 *
 * Ak to nie je nutne, EXISTUJUCE STRUKTURY A FUNKCIE NEMENIT!
 *  Novy kod dajte do vlastneho zdrojoveho/hlavickoveho suboru
 *  Pripadne zmeny v povodnych suboroch oznacte komentarmi s vasimi inicialkami
 *  Piste prehladny a citatelny kod, pomenuvajte premenne celymi nazvami
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "isle.h"
#include "random.h"

void testPoleOstrovov(  Isle *  p_isle, int poc_ostrovov)
{
    int year, action, error;
    int emigrants;
    int i;
    
    printf("Starting out...\n ");
    
    for (year = 1; year <= 100; year++)
    {        
      for(i=0; i< poc_ostrovov ; i++) {
      
        debug_print_isle(stdout, p_isle+i);
//        action = get_uniform(0, NUM_ACTIONS-1);

        printf("Zadaj Akciu: \n");
        printf("build_farm 0,  build_village 1, chop_forest 2, gather_wood 3, \n");
        printf("gather_food 4, go_fishing 5,    build_moai 6,  do_nothing 7 \n");
        printf("exit 9 \n");
    	scanf("%d", &action );
    	if(action == 9 ) return;

        printf("Year %i:\n ", year);
  //      printf("akcia %i:\n ", action);
        printf("Villagers decided to %s, and it was ", VillageActionNames[action]);
        error = VillageActions[action](p_isle+i);
        printf("%s.\n", error == ERR_OK ? "done" : "impossible");
             
        emigrants = yearly_update(p_isle+i);

        debug_print_isle(stdout, p_isle+i);
        printf("%i people emigrated.\n", emigrants);
        printf("\n");
        
      }
      printf("\n\n");
    }
}


int canBuildFarm(Isle * pIsle)
{
    if (pIsle->pop < GS(PopPerVillage)+GS(PopPerFarm))
        return 0;

    if (pIsle->wood < LS(WoodPerFarm))
        return 0;

    if(pIsle->farms > 30) return 0;

    return 1;
}

int canBuildMoai(Isle * pIsle)
{
    if (pIsle->pop < GS(PopPerVillage)*GS(MoaiMultiplier))
        return 0;

    if (pIsle->wood < GS(WoodPerVillage)*GS(MoaiMultiplier))
        return 0;

    return 1;
}

int canBuildVilage(Isle * pIsle)
{
    if (pIsle->pop < GS(PopPerVillage)*pIsle->villages + GS(PopPerVillage)/2)
        return 0;

    if (pIsle->wood < GS(WoodPerVillage))
        return 0;

    if(pIsle->villages > 4)
    	return 0;

    return 1;
}

int canChopForest(Isle * pIsle)
{
	if(pIsle->forests > 51) return 1;
	return 0;
}

int doOther(Isle * pIsle)
{
	if(get_alternative(50)) return 3;
	if(get_alternative(50)) return 5;
	if(canChopForest(pIsle) && get_alternative(50) ) return 2;

	return 4;
}
//va_build_farm,  va_build_village, va_chop_forest, va_gather_wood,
//va_gather_food, va_go_fishing,    va_build_moai,  va_do_nothing,


int calculate_action( Isle * p_isle, int oldaction )
{

	if( canBuildMoai(p_isle) ) return  6;
	if( canBuildFarm (p_isle) ) return 0;
	if( canBuildVilage (p_isle) ) return 1;

	if(p_isle->wood < 100) return 3;

	if(p_isle->food < p_isle->pop && p_isle->pop < 550) return 4;

//	if(get_alternative(80)) return get_uniform(0, NUM_ACTIONS-1);

	return  doOther(p_isle);
}

void testNotRandomPoleOstrovov(  Isle*  p_isle, int poc_ostrovov)
{
    int year,  error;
    int action[poc_ostrovov];
    int emigrants;
    int i;

    for(i=0;i<poc_ostrovov;i++) action[i]=0;

    printf("Starting out...\n ");
    
    for (year = 1; year <= 100; year++)
    {

   	 for(i=0; i< poc_ostrovov ; i++) {

         debug_print_isle(stdout, p_isle+i);
     	   	 action[i]= calculate_action( p_isle+i, action[i] );

    //	  calculate_action( p_isle[i] )
        //debug_print_isle(stdout, p_isle[i]);
//          action = get_uniform(0, NUM_ACTIONS-1);

        printf("Year %i:\n ", year);
        printf("Villagers decided to %s, and it was ", VillageActionNames[action[i]]);
        error = VillageActions[action[i]](p_isle+i);
        printf("%s.\n", error == ERR_OK ? "done" : "impossible");

        emigrants = yearly_update(p_isle+i);

        debug_print_isle(stdout, p_isle+i);
        printf("%i people emigrated.\n", emigrants);
        printf("\n");

      }
      printf("\n\n");
    }
}


// tato funkcia vygeneruje jeden ostrov s nahodnymi hodnotami, vrati smernik na ostrov
void   createRandom_island(Isle* p_isle)
 {
    
	printf("Zadaj meno ostrova: ");
	scanf("%s",p_isle->name);
	p_isle->forests  = get_uniform(1, 55);  
	p_isle->villages = get_uniform(1, 3);
	p_isle->farms = get_uniform(1, 15);
	p_isle->pop  = get_uniform(1, 90);
	p_isle->rats = get_uniform(1, 3);
	p_isle->food = get_uniform(1, 7);
	p_isle->wood = get_uniform(1, 7);
	p_isle->moai = 0; 
	p_isle->pSettings = &GlobalSettings;
        
  //  Isle isle = {"Testia", 50, 1, 10, 100, 0, 0, 0, 0, &GlobalSettings};

}

// tato funkcia vygeneruje pole ostrovov,
//kazdy ostrov v poli sa vytvori funkciou s nahodnymi hodnotami
Isle* create_islands(int N)
{
	int i;

	Isle* p_isle = (Isle*) calloc(N , sizeof(Isle));

	for(i=0; i<N; i++)
    {
		createRandom_island(p_isle+i);
    }

    return p_isle;
}


char *readItem(FILE *file) {

    if (file == NULL) {
        printf("Error: file pointer is null.");
        exit(1);
    }

    int maximumLineLength = 128;
    char *lineBuffer = (char *)malloc(sizeof(char) * maximumLineLength);

    if (lineBuffer == NULL) {
        printf("Error allocating memory for line buffer.");
        exit(1);
    }

    char ch = getc(file);
    int count = 0;

    while ((ch != ',') && (ch != '\n') && (ch != EOF)) {
        if (count == maximumLineLength) {
            maximumLineLength += 128;
            lineBuffer = realloc(lineBuffer, maximumLineLength);
            if (lineBuffer == NULL) {
                printf("Error reallocating space for line buffer.");
                exit(1);
            }
        }
        lineBuffer[count] = ch;
        count++;

        ch = getc(file);
    }

    lineBuffer[count] = '\0';

    return lineBuffer;
  //  char line[count + 1];
 //   strncpy(line, lineBuffer, (count + 1));
 //   free(lineBuffer);
//    const char *constLine = line;
 //   return constLine;
}

void loadIslandFromFile(FILE  *fpData , Isle* p_isle)
{
	char *  str = readItem( fpData);
	strcpy(p_isle->name,  str );
	free( str );

	str = readItem( fpData);
	p_isle->forests  = atoi(str);
	free( str );

	str = readItem( fpData);
	p_isle->villages = atoi(str );
	free( str );

	str = readItem( fpData);
	p_isle->farms = atoi(str );
	free( str );

	str = readItem( fpData);
	p_isle->pop  = atoi(str );
	free( str );

	str = readItem( fpData);
	p_isle->rats = atoi(str );
	free( str );

	str = readItem( fpData);
	p_isle->food = atoi(str );
	free( str );

	str = readItem( fpData);
	p_isle->wood = atoi(str );
	free( str );

	str = readItem( fpData);
	p_isle->moai = atoi(str );
	free( str );

	p_isle->pSettings = &GlobalSettings;

}

Isle* loadIslandsFromFile(int N)
{
	int i;
	FILE    *fpData;

	if ( ( fpData = fopen( "input.csv", "r" ) ) == NULL ) //Reading a file
	{
	    printf( "File could not be opened.\n" );
	    return NULL;
	}

	Isle* p_isle = (Isle*) calloc(N , sizeof(Isle));

	for(i=0; i<N; i++)
    {
		loadIslandFromFile(fpData , (p_isle+i));
    }

	fclose(fpData);

    return p_isle;
}

void  writeIslandToFile( FILE *fp,  Isle* p_isle )
{
	const char * comma = ",";
	fprintf(fp, "%s", p_isle->name);
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->forests  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->villages  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->farms  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->pop  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->rats  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->food  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->wood  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "%i", p_isle->moai  );
	fprintf(fp, "\n");
}

void createInputFileIslads(int N)
{
	Isle isle1 = {"Maui", 60, 3, 13, 115, 0, 0, 0, 0, &GlobalSettings};
	Isle isle2 = {"Kauai", 50, 5, 10, 105, 0, 0, 0, 0, &GlobalSettings};
	Isle isle3 = {"Oahu", 40, 1, 8, 80, 501, 0, 0, 0, &GlobalSettings};

	FILE *fp;
	fp=fopen("input.csv", "w");

	writeIslandToFile(fp,&isle1);
	writeIslandToFile(fp,&isle2);
	writeIslandToFile(fp,&isle3);

	Isle p_isle;

	int i;
	for(i=3; i<N; i++)
    {
		createRandom_island(&p_isle);
		writeIslandToFile(fp, &p_isle);
    }

	fclose(fp);

}

void  writeIslandDetailsToFile( FILE *fp,  Isle* p_isle )
{
	const char * comma = "\n";
	fprintf(fp, "Nazov Ostrova:\t %s", p_isle->name);
	fprintf(fp, "%s",comma);
	fprintf(fp, "Pocet Lesov:\t %i", p_isle->forests  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Pocet Dedin:\t %i", p_isle->villages  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Pocet Fariem:\t %i", p_isle->farms  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Populacia:\t %i", p_isle->pop  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Rats:\t %i", p_isle->rats  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Jedlo:\t %i", p_isle->food  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Drevo:\t%i", p_isle->wood  );
	fprintf(fp, "%s",comma);
	fprintf(fp, "Moai:\t %i", p_isle->moai  );
	fprintf(fp, "\n\n");
}

void writeOutputFileIslands(const char* fileName, Isle* p_isle, int N)
{
	FILE *fp;
	fp=fopen(fileName, "w");

	int i;
	for(i=0; i<N; i++)
    {
		writeIslandDetailsToFile(fp, p_isle+i );
    }

	fclose(fp);

}

void nahodneOstrovy(int pocet_ostrovov)
{
    Isle* ostrovy = create_islands(pocet_ostrovov);
    testPoleOstrovov(ostrovy, pocet_ostrovov);
    writeOutputFileIslands( "outputrandom.txt", ostrovy, pocet_ostrovov );
    free(ostrovy);

}

int main()
{
	int pocet_ostrovov = 8;
    
    init_random_seed();
    
 //   nahodneOstrovy(pocet_ostrovov);


    createInputFileIslads(pocet_ostrovov);
    Isle* ostrovyFile = loadIslandsFromFile(pocet_ostrovov);
    if(ostrovyFile == NULL) {
    	printf("Error Reading Islands From File!!!\n");
   		return 1;
    }
    testNotRandomPoleOstrovov(ostrovyFile, pocet_ostrovov);
    writeOutputFileIslands( "output.txt", ostrovyFile, pocet_ostrovov );


    free(ostrovyFile);

//    system("pause");
    return 0;
}
